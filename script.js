passwordForm.onclick = function(event){
    let targetElement = event.target;
    if(targetElement.id == "inputPassword_view"){
        let passTarget = document.querySelector(`#inputPassword_view.fa-eye`)
        if(passTarget != null){
            passTarget.classList.remove("fa-eye");
            passTarget.classList.add("fa-eye-slash");
            inputPassword.type ='text'
        }else{
            targetElement.classList.remove("fa-eye-slash");
            targetElement.classList.add("fa-eye");
            targetElement.type ="password";
            inputPassword.type ='password'
        }
    }else if(targetElement.id == "repeatPassword_viev"){
        let passTarget = document.querySelector(`#repeatPassword_viev.fa-eye`)
        if(passTarget != null){
            targetElement.classList.remove("fa-eye");
            targetElement.classList.add("fa-eye-slash");
            repeatPassword.type ='text'
        }else{
            targetElement.classList.remove("fa-eye-slash");
            targetElement.classList.add("fa-eye");
            targetElement.type ="password";
            repeatPassword.type ='password'
        }
    }else if(targetElement.className == "btn"){
        submitEvent();
    }
}
function submitEvent(){
    let password = document.querySelector("#inputPassword");
    let repeat = document.querySelector("#repeatPassword");
    if(password.value != repeat.value){
        let checkWrong = document.querySelector('.wrongpas');
        if(checkWrong == null){
            let wrongPassword = document.createElement('p');
            wrongPassword.textContent='Потрібно ввести однакові значення';
            wrongPassword.classList.add('wrongpas')
            let point = document.querySelector('#repeatPassword')
            point.after(wrongPassword);
        }
        return false;
    }else{
        let wrong = document.querySelector('.wrongpas')
        if(wrong != null){
            wrong.remove();
        }
        alert('You are welcome')
    }
}